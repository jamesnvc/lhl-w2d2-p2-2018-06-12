//
//  ViewController.m
//  AutoLayoutDemo
//
//  Created by James Cash on 12-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pinkLeadingConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *v1 = [[UIView alloc] initWithFrame:CGRectZero];
    v1.backgroundColor = UIColor.magentaColor;
    v1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:v1];

    /*
    [NSLayoutConstraint constraintWithItem:v1
                                 attribute:NSLayoutAttributeCenterX
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.view
                                 attribute:NSLayoutAttributeCenterX
                                multiplier:1.0
                                  constant:0].active = YES;
     */
    [v1.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [v1.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor constant:50].active = YES;
    [v1.widthAnchor constraintEqualToConstant:50].active = YES;
    NSLayoutConstraint *heightConstraint = [v1.heightAnchor constraintEqualToConstant:50];
    heightConstraint.active = YES;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePinkSize:(id)sender {
    NSInteger newLeading = arc4random_uniform(100);
    [UIView animateWithDuration:1 animations:^{
        self.pinkLeadingConstraint.constant = newLeading;
        // to make the animation pick up the changes that result from updating the constraint, we need to run the layout engine
        // if we don't do this then the animation block doesn't actually see any view properties changing; although the system of constraints changed, it doesn't actually change the views until the layout engine gets another chance to run
        [self.view layoutSubviews];
    }];
}

@end
